'use strict';
var pg = require('pg');
const fastcsv = require("fast-csv");
var nodemailer = require('nodemailer');

var aws = require("aws-sdk");
var ses = new aws.SES();

exports.handler = function (event, context,callback) {

    var dbConfig = {
        user: '<>',
        password: '<>',
        database: '<>',
        host: '<>',
        client_encoding: 'latin1',
    };

    // create Nodemailer SES transporter
    var transporter = nodemailer.createTransport({
        SES: ses
    });

    const query = `SELECT NOW() as time`;

    var client = new pg.Client(dbConfig);
    try {
        client.connect();
        client.query(query).then(result => { 
            const jsonData = JSON.parse(JSON.stringify(result.rows));

            // write csv
            fastcsv.writeToString(jsonData, { headers: true, delimiter: ",", encoding: "latin1" }).then((res)=>{
                // generate email
                var mailOptions = {
                    from: "abc@abc.com", // SES Email address
                    subject: "Report 1",
                    html: `Hi, here is your report`,
                    to: "me@icloud.com",
                    attachments: [
                        {
                            filename: 'report1.csv',
                            content: res,
                            contentType: 'text/csv',
                            encoding: 'latin1'
                        }
                    ]
                };

                // send email
                transporter.sendMail(mailOptions, function (err, info) {
                    if (err) {
                        console.log("Error sending email " + err);
                        callback(err);
                    } else {
                        console.log("Email sent successfully");
                        callback();
                    }
                });
            });

            client.end();
            callback(null,"Success");
          }).catch(e => {  console.error(e); callback(e); } );
    }
    catch (err) {
        console.log(err);
        client.end();
        callback(err)
    }
};